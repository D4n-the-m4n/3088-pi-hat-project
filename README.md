# 3088 Pi Hat Project

This micro-Pi HAT functions as an Uninterrupted Power Supply (UPS).

Subsystems required:
- Voltage Supply and Battery Charging Circuit
- Current Shunt
- LED status lights

Requirements:
- UPS micro-Pi HAT must recieve 12V supply.
- This 12V supply must be stepped down to provided a 5V output.
- The circuitry must also charge the battery.
- The HAT requires 3 LED indicators: a battery full, battery at 25% and a battery charging indicator.
- The design must meet the Raspberry Pi foundations Standards for HATS https://github.com/raspberrypi/hats
